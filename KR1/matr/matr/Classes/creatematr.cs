﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matr.Classes
{
    public class creatematr
    {
        public int[,] mas1;
        public int[,] mas2;
        Random rand = new Random();
        public creatematr(int n, int m)
        {
            mas1 = new int[n,m];
            mas2 = new int[n,m];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    mas1[i, j] = rand.Next(0, 100);
                    mas2[i, j] = rand.Next(0, 100);
                }
            }
        }
        public void Show()
        {
            for (int i = 0; i <5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(mas1[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(mas2[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

    }
}

