﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matr.Classes
{
    class Mnojka : Interf
    {
        int _n;
        int _m;
        public creatematr matrix;
        public int[,] mas;
        public Mnojka(int n, int m, creatematr am)
        {
            _n = n;
            _m = m;
            matrix = am;
            mas = new int[_n, _m];
        }
        int Interf.n
        {
            get
            {
                return _n;
            }

            set
            {
                value = _n;
            }
        }

        int Interf.m
        {
            get
            {
                return _m;
            }

            set
            {
                value = _m;
            }
        }

        public double functin()
        {
            int t = 0;
            for (int i = 0; i < _n; i++)
            {
                for (int j = 0; j < _m; j++)
                {
                    
                    for (int k=0; k< _m;k++)
                    { 
                        t += matrix.mas1[i, k] * matrix.mas2[k,j];
                    }
                    mas[i, j] = t;
                    t = 0;

                }
            }
            return 0;
        }
    }
}
