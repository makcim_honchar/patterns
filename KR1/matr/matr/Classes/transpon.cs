﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matr.Classes
{
    class transpon : Interf
    {
        int _n;
        int _m;
        public creatematr matrix;
        public int[,] mas;
        public transpon(int n, int m, creatematr ak)
        {
            _n = n;
            _m = m;
            matrix = ak;
            mas = new int[_n, _m];
        }
        int Interf.n
        {
            get
            {
                return _n;
            }

            set
            {
                value = _n;
            }
        }

        int Interf.m
        {
            get
            {
                return _m;
            }

            set
            {
                value = _m;
            }
        }
       
        public double functin()
        {
           
            for (int i = 0; i < _n; i++)
            {
                for (int j = 0; j < _m; j++)
                {
                    mas[i, j] = matrix.mas2[j, i];
                    
                }
               
            }
            
            return 0;
        }
        public void Shows()
        {
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(matrix.mas2[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

    }

}