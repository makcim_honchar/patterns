﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matr.Classes
{
    class delenie : Interf
    {
        int _n;
        int _m;
        public creatematr matrix;
        public int[,] mas;
        public int[,] mss;
        public delenie(int n, int m, creatematr ak, int [,] mass)
        {
            _n = n;
            _m = m;
            matrix = ak;
            mas = new int[_n, _m];
            mss = mass;
        }
        int Interf.n
        {
            get
            {
                return _n;
            }

            set
            {
                value = _n;
            }
        }

        int Interf.m
        {
            get
            {
                return _m;
            }

            set
            {
                value = _m;
            }
        }
        


        public double functin()
        {
            int t = 0;
            for (int i = 0; i < _n; i++)
            {
                for (int j = 0; j < _m; j++)
                {

                   for (int k = 0; k < _m; k++)
                    {
                        t += matrix.mas1[i, k] * mss[k, j];
                    }
                    mas[i, j] = t;
                    t = 0;

                }
            }
            return 0;
        }
    }
}