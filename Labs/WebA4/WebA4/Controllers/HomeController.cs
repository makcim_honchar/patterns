﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Patterns.Clases;
using WebA4.Models;

namespace WebA4.Controllers
{

    class ComparerHourlyPay : IComparer<HourlyPay>
    {
        public int Compare(HourlyPay x, HourlyPay y)
        {
            if (x.Payment() > y.Payment())
            {
                return 1;
            }
            else if (x.Payment() < y.Payment())
            {
                return -1;
            }
            else return 0;
        }
    }
    class ComparerSinglePay : IComparer<SinglePay>
    {
        public int Compare(SinglePay x, SinglePay y)
        {
            if (x.Payment() > y.Payment())
            {
                return 1;
            }
            else if (x.Payment() < y.Payment())
            {
                return -1;
            }
            else return 0;
        }
    }

    public class HomeController : Controller
    {

        RdContext db = new RdContext();

        public ActionResult Index()
        {

            List<SinglePay> s = new List<SinglePay>();
            
            s.Add(new Patterns.Clases.SinglePay(1, "Тарасов И.И.", 250));
            s.Add(new Patterns.Clases.SinglePay(2, "Титанов В.А.", 350));
            s.Add(new Patterns.Clases.SinglePay(3, "Мартов А.В.", 190));
            s.Add(new Patterns.Clases.SinglePay(4, "Лукас Н.М.", 197));
            s.Add(new Patterns.Clases.SinglePay(5, "Калина А.В.", 293));
            s.Add(new Patterns.Clases.SinglePay(6, "Колумб С.И.", 425));
            s.Add(new Patterns.Clases.SinglePay(7, "Зозульева О.В.", 750));
            s.Add(new Patterns.Clases.SinglePay(8, "Шматко И.В.", 950));
            s.Add(new Patterns.Clases.SinglePay(9, "Шадов И.С.", 320));
            s.Add(new Patterns.Clases.SinglePay(10, "Григоренко М.В.", 900));

            s.Sort(new ComparerSinglePay());

            ViewBag.SinglePays = s;

            List<HourlyPay> s1 = new List<HourlyPay>();
            s1.Add(new Patterns.Clases.HourlyPay(1, "Петров И.И.", 5));
            s1.Add(new Patterns.Clases.HourlyPay(2, "Мальков В.А.", 2.21));
            s1.Add(new Patterns.Clases.HourlyPay(3, "Михно А.В.", 1.9));
            s1.Add(new Patterns.Clases.HourlyPay(4, "Луньова Н.М.", 1.7));
            s1.Add(new Patterns.Clases.HourlyPay(5, "Калина А.В.", 293));
            s1.Add(new Patterns.Clases.HourlyPay(6, "Кривошлик С.И.", 1.01));
            s1.Add(new Patterns.Clases.HourlyPay(7, "Запутряева О.В.", 0.9));
            s1.Add(new Patterns.Clases.HourlyPay(8, "Шевченко И.В.", 3.2));
            s1.Add(new Patterns.Clases.HourlyPay(9, "Шаповал И.С.", 4.1));
            s1.Add(new Patterns.Clases.HourlyPay(10, "Гончар М.В.", 7.2));
            //foreach (HourlyPay a in s1) { a.Payment(); }
            s1.Sort(new ComparerHourlyPay());

            ViewBag.HourlyPays = s1;
                        return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}