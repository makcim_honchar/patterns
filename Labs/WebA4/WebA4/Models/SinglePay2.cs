﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class SinglePay2:Creator
    {
        public override MainClass FactoryMethod(int id, string fio, double pay)
        {
            return new SinglePay(id,fio,pay);
        }
    }
}
