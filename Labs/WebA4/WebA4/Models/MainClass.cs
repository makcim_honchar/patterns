﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
 public abstract class MainClass
    {
        public int Id { get; set; }
        public string Fio { get; set; }        
        public abstract double Payment();
        public MainClass(int id, string fio)
        {
            Id = id;
            Fio = fio;
        }
    }
}
