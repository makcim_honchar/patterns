﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Patterns.Clases;
using WebA4.Models;

namespace WebA4.Models
{
    public class RdContext:DbContext
    {
       
        public DbSet<MainClass> MainClasse { get; set; }

        public DbSet<HourlyPay> HourlyPayss { get; set; }

        public DbSet<SinglePay> SinglePayss { get; set; }

    }
}