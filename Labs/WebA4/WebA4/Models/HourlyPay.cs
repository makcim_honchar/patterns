﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
   public class HourlyPay:MainClass
    {
        
        public double PaymentForHour { get; set; }
        public HourlyPay(int id, string fio, double paymentForHour):base(id,fio)
        {            
            PaymentForHour = paymentForHour;
        }
        public override double Payment()
        {
            return 4 * 8 * 5 * PaymentForHour;
        }
    }
}
