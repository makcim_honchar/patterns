﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
   public class SinglePay : MainClass
    {    
         
        public double SinglePayment { get; set; }
        public SinglePay(int id, string fio, double singlePayment):base(id,fio)
        {
            
            SinglePayment = singlePayment;
        }
        public override double Payment()
        {
            return SinglePayment;
        }
    }
}
