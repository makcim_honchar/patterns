﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class HourlyPay1 : Creator
    {
        public override MainClass FactoryMethod(int id,string fio,double pay)
        {
            return new HourlyPay(id,fio,pay);
        }
    }
}
