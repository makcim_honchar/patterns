﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    abstract class  Creator
    {
        public abstract MainClass FactoryMethod(int id,string fio,double pay);
    }
}
