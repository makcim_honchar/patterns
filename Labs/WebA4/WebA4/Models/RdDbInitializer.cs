﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Patterns.Clases;
using WebA4.Models;

namespace WebA4.Models
{
    public class RdDbInitializer : DropCreateDatabaseAlways<RdContext>
    {
        protected override void Seed(RdContext db)
        {

            db.SinglePayss.Add(new Patterns.Clases.SinglePay(1, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(2, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(3, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(4, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(5, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(6, "ввв", 555));
            db.SinglePayss.Add(new Patterns.Clases.SinglePay(7, "ввв", 555));
            //db.HourlyPayss.Add(new HourlyPays { id = "Война и мир", fio = "Л. Толстой", PaymentForHour = 220 });
            //db.HourlyPayss.Add(new HourlyPays { id = "Отцы и дети", fio = "И. Тургенев", paymentForHour = 180 });
            //db.HourlyPayss.Add(new HourlyPays { id = "Чайка", fio = "А. Чехов", paymentForHour = 150 });

            base.Seed(db);
        }
    }
}