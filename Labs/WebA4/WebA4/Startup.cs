﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebA4.Startup))]
namespace WebA4
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
