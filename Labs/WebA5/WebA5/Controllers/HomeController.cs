﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Patterns.Clases;
using WebA5.Models;

namespace WebA5.Controllers
{
    class ComparerHourlyPay : IComparer<HourlyPay>
    {
        public int Compare(HourlyPay x, HourlyPay y)
        {
            if (x.CalculatePayment() > y.CalculatePayment())
            {
                return 1;
            }
            else if (x.CalculatePayment() < y.CalculatePayment())
            {
                return -1;
            }
            else return 0;
        }
    }
    class ComparerSinglePay : IComparer<SinglePay>
    {
        public int Compare(SinglePay x, SinglePay y)
        {
            if (x.CalculatePayment() > y.CalculatePayment())
            {
                return 1;
            }
            else if (x.CalculatePayment() < y.CalculatePayment())
            {
                return -1;
            }
            else return 0;
        }
    }
    public class HomeController : Controller
    { 
        public ActionResult Index()
        {
            ViewData["Head"] = "Привет мир!";
            if (ViewData["Head"] == null)
            {
                ViewData["Head"] = "Laba";
            }

            IntermediateClass IC = new IntermediateClass(10, "Писаренко О.Д.", 10);
            HourlyPay hp = new HourlyPay(1, "Петров И.И.", 5, IC);
            SinglePay sp = new SinglePay(1, "Тарасов И.И.", 250, IC);
           
            IC.NotifyObservers();
            
            IC.Change(111, "Мирошниченко В.П.", 888);
           
            

            List<SinglePay> s = new List<SinglePay>();
            s.Add(new SinglePay(1, "Петров И.И.", 5,IC));
            s.Add(new SinglePay(2, "Петров A.И.", 15, IC));

            s.Sort(new ComparerSinglePay());
            ViewBag.SinglePay = s;

            List<HourlyPay> s1 = new List<HourlyPay>();
            s1.Add(new HourlyPay(1, "Карасев И.И.", 5, IC));
            s1.Add(new HourlyPay(2, "Понтифий A.И.", 15, IC));
            s1.Sort(new ComparerHourlyPay());
            ViewBag.HourlyPay= s1;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}