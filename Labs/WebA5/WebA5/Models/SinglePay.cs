﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class SinglePay : MainClass,IObserver
    {       
        
        IObservable obs;
        public SinglePay(int id, string fio,double payment, IObservable obs1) : base(id, fio,payment)
        {            
            obs = obs1;
            obs.RegisterObserver(this);
        }
        public override double CalculatePayment()
        {
            return Payment;
        }

        public void Update(MainClass ob)
        {
            Id = ob.Id;
            Fio = ob.Fio;
            Payment = ob.Payment;
        }
        public void StopTrade()
        {
            obs.RemoveObserver(this);
            obs = null;
        }
    }
}
