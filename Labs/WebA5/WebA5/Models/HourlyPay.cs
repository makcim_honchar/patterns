﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
     class HourlyPay:MainClass,IObserver
    {
        IObservable obs;
        public HourlyPay(int id,string fio,double payment,IObservable obs1):base(id,fio,payment)
        {            
            obs = obs1;
            obs.RegisterObserver(this);
        }
        public override double CalculatePayment()
        {
            return 4 * 8 * 5 * Payment;
        }

        public void Update(MainClass ob)
        {
            Id = ob.Id;
            Fio = ob.Fio;
            Payment = ob.Payment / 120;
        }
        public void StopTrade()
        {
            obs.RemoveObserver(this);
            obs = null;
        }
    }
}
