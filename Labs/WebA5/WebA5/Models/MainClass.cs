﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    public class MainClass
    {
        public int Id { get; set; }
        public string Fio { get; set; }
        public double Payment { get; set; }
        public virtual double CalculatePayment() { return 0; }
        public MainClass(int id, string fio,double payment)
        {
            Id = id;
            Fio = fio;
            Payment = payment;
        }       
    }
}
