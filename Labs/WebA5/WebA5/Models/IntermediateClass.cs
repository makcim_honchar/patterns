﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class IntermediateClass : IObservable
    {
        MainClass obj;
        List<IObserver> observers;
        public IntermediateClass(int id1,string fio1,double payment1)
        {
            observers = new List<IObserver>();
            obj = new MainClass(id1, fio1,payment1);
                  
        }
        public void NotifyObservers()
        {
            foreach (IObserver o in observers)
            {
                o.Update(obj);
            }
        }

        public void RegisterObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }                 
        public void Change(int id1, string fio1, double payment1)
        {
            obj = new MainClass(id1, fio1, payment1);
            NotifyObservers();
        }
    }
}
