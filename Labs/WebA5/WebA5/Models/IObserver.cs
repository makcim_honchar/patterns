﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    interface IObserver
    {
        void Update(MainClass ob);
    }
}
