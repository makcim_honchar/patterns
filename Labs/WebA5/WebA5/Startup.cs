﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebA5.Startup))]
namespace WebA5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
