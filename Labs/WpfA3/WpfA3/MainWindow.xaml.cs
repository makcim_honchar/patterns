﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Patterns.Clases;

namespace WpfA3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<HourlyPay> nya;
        List<SinglePay> nyas;
        public MainWindow()
        {
            InitializeComponent();
            nya = new List<HourlyPay>();
            create(nya);

            nya.Sort(new ComparerHourlyPay());

            nyas = new List<SinglePay>();
            creates(nyas);

            nyas.Sort(new ComparerSinglePay());


        }
      
      
        //Sortirovka
        class ComparerHourlyPay : IComparer<HourlyPay>
        {
            public int Compare(HourlyPay x, HourlyPay y)
            {
                if (x.Payment() > y.Payment())
                {
                    return 1;
                }
                else if (x.Payment() < y.Payment())
                {
                    return -1;
                }
                else return 0;
            }
        }
        ////Zapolnenie
        private static void create(List<HourlyPay> nya)
        {
            nya.Add(new HourlyPay(1, "Петров И.И.", 5));
            nya.Add(new HourlyPay(2, "Мальков В.А.", 2.21));
            nya.Add(new HourlyPay(3, "Михно А.В.", 1.9));
            nya.Add(new HourlyPay(4, "Луньова Н.М.", 1.7));
            nya.Add(new HourlyPay(5, "Кутоманова А.В.", 2.3));
            nya.Add(new HourlyPay(6, "Кривошлик С.И.", 1.01));
            nya.Add(new HourlyPay(7, "Запутряева О.В.", 0.9));
            nya.Add(new HourlyPay(8, "Шевченко И.В.", 3.2));
            nya.Add(new HourlyPay(9, "Шаповал И.С.", 4.1));
            nya.Add(new HourlyPay(10, "Гончар М.В.", 7.2));
        }


        class ComparerSinglePay : IComparer<SinglePay>
        {
            public int Compare(SinglePay x, SinglePay y)
            {
                if (x.Payment() > y.Payment())
                {
                    return 1;
                }
                else if (x.Payment() < y.Payment())
                {
                    return -1;
                }
                else return 0;
            }
        }
        //Zapolnenie
        private static void creates(List<SinglePay> nya)
        {
            nya.Add(new SinglePay(1, "Тарасов И.И.", 250));
            nya.Add(new SinglePay(2, "Титанов В.А.", 350));
            nya.Add(new SinglePay(3, "Мартов А.В.", 190));
            nya.Add(new SinglePay(4, "Лукас Н.М.", 197));
            nya.Add(new SinglePay(5, "Калина А.В.", 293));
            nya.Add(new SinglePay(6, "Колумб С.И.", 425));
            nya.Add(new SinglePay(7, "Зозульева О.В.", 750));
            nya.Add(new SinglePay(8, "Шматко И.В.", 950));
            nya.Add(new SinglePay(9, "Шадов И.С.", 320));
            nya.Add(new SinglePay(10, "Григоренко М.В.", 900));
        }
        public class Facade
        {
            
            List<HourlyPay> nya;
            List<SinglePay> nyas;

            public Facade()
            {

                nya = new List<HourlyPay>();
                nyas = new List<SinglePay>();
                create(nya);
                creates(nyas);
              
            }

            public void MethodA(DataGrid dataGrid)
            {
                
                nyas.Sort(new ComparerSinglePay());
                dataGrid.ItemsSource = null;
                dataGrid.ItemsSource = nyas;

            }

            public void MethodB(DataGrid dataGrid1)
            {

                nya.Sort(new ComparerHourlyPay());
                dataGrid1.ItemsSource = null;
                dataGrid1.ItemsSource = nya;
            }


        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Facade n = new Facade();
            n.MethodA(dataGrid);
        }

        private void button_Click1(object sender, RoutedEventArgs e)
        {
            Facade n = new Facade();
            n.MethodB(dataGrid1);
        }
    }
} 
