﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class HourlyPay:MainClass
    {
        private int _id;
        private string _fio;
        public double PaymentForHour { get; set; }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Fio
        {
            get
            {
                return _fio;
            }

            set
            {
                _fio = value;
            }
        }

        public HourlyPay(int id,string fio,double paymentForHour)
        {
            _id = id;
            _fio = fio;
            PaymentForHour = paymentForHour;
        }
        public double Payment()
        {
            return 4 * 8 * 5 * PaymentForHour;
        }        
    }
}
