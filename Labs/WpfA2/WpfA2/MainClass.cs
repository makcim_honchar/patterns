﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    interface MainClass
    {
        int Id {get; set;}
        string Fio { get; set; }        
        double Payment(); 
           
    }
}
