﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Clases
{
    class SinglePay : MainClass
    {
        private int _id;
        public string _fio;
        public double SinglePayment { get; set; }

        public int Id
        {
            get
            {
                return _id;
            }

            set
           {
                 _id=value;
            }
        }

        public string Fio
        {
            get
            {
                return _fio;
            }

            set
            {
                _fio=value;
            }
        }

        public SinglePay(int id, string fio, double singlePayment)
        {
            _id = id;
            _fio = fio;
            SinglePayment = singlePayment;
        }

        public double Payment()
        {
            return SinglePayment;
        }
    }
}
